<?php

use Illuminate\Database\Seeder;
use App\Models\Permissions;
use Illuminate\Support\Facades\DB;
use App\Models\Roles;

class PermissionsTableSeeder extends Seeder
{
    public function run()
    {
        //----------------------------------------------------
        DB::table('permissions')->delete();
        //----------------------------------------------------

        $permissionShow1 = Permissions::create([
            'name'                 =>'Panel.Main.index',
            'label'                =>'Pode Visualizar o Panel do Sistema',
            'group'                =>'Sistema',
            'system'               =>1,
            'default'              =>1,
        ]);
        $permissionShow1->roles()->sync(Roles::where('name', '!=', 'Administrator')->get());
        //----------------------------------------------------
        $permissions =
            [
                //Users--------------------------------
                [
                    'ambient'     =>'Panel',
                    'name'        =>'Users',
                    'group'       =>'Usuários',
                ],
                //Roles--------------------------------
                [
                    'ambient'     =>'Panel',
                    'name'        =>'Roles',
                    'group'       =>'Funções',
                ],
                //Permissions--------------------------
                [
                    'ambient'     =>'Panel',
                    'name'        =>'Permissions',
                    'group'       =>'Permissões',
                ],
            ];
        //----------------------------------------------------
        foreach ($permissions as $p)
        {
            $name = $p['name'];
            $ambient = $p['ambient'];

            $validationUniqueList1 =  Permissions::where('name', "$ambient.$name.index")->get()->first();
            if($validationUniqueList1==null) {
                $permissionShow2 = Permissions::create([
                    'name' => $p['ambient'] . '.' . $p['name'] . '.index',
                    'label' => "Pode Acessar Lista ou item de " . $p['name'],
                    'group' => $p['group'],
                    'system' => 1,
                ]);
            }
            $validationUniqueList2 =  Permissions::where('name', "$ambient.$name.create")->get()->first();
            if($validationUniqueList2==null) {
                $permissionShow3 = Permissions::create([
                    'name'                 =>$p['ambient'].'.'.$p['name'].'.create',
                    'label'                =>"Pode Visualizar a Tela de Cadastro ".$p['name'],
                    'group'                =>$p['group'],
                    'system'               =>1,
                ]);
            }
            $validationUniqueList3 =  Permissions::where('name', "$ambient.$name.edit")->get()->first();
            if($validationUniqueList3==null) {
                $permissionShow4 = Permissions::create([
                    'name'                 =>$p['ambient'].'.'.$p['name'].'.edit',
                    'label'                =>"Pode Visualizar a Tela de Editar ".$p['name'],
                    'group'                =>$p['group'],
                    'system'               =>1,
                ]);
            }
            $validationUniqueList4 =  Permissions::where('name', "$ambient.$name.show")->get()->first();
            if($validationUniqueList4==null) {
                $permissionShow5 = Permissions::create([
                    'name'                 =>$p['ambient'].'.'.$p['name'].'.show',
                    'label'                =>"Pode Visualizar Item único de ".$p['name'],
                    'group'                =>$p['group'],
                    'system'               =>1,
                ]);
            }
            $validationUniqueList5 =  Permissions::where('name', "$ambient.$name.store")->get()->first();
            if($validationUniqueList5==null) {
                $permissionShow6 = Permissions::create([
                    'name'                 =>$p['ambient'].'.'.$p['name'].'.store',
                    'label'                =>"Pode Cadastrar ".$p['name'],
                    'group'                =>$p['group'],
                    'system'               =>1,
                ]);
            }
            $validationUniqueList6 =  Permissions::where('name', "$ambient.$name.update")->get()->first();
            if($validationUniqueList6==null) {
                $permissionShow7 = Permissions::create([
                    'name'                 =>$p['ambient'].'.'.$p['name'].'.update',
                    'label'                =>"Pode Editar ".$p['name'],
                    'group'                =>$p['group'],
                    'system'               =>1,
                ]);
            }
            $validationUniqueList7 =  Permissions::where('name', "$ambient.$name.delete")->get()->first();
            if($validationUniqueList7==null) {
                $permissionShow8 = Permissions::create([
                    'name'                 =>$p['ambient'].'.'.$p['name'].'.delete',
                    'label'                =>"Pode Deletar ".$p['name'],
                    'group'                =>$p['group'],
                    'system'               =>1,
                ]);
            }
        }
        //----------------------------------------------------
    }
}
