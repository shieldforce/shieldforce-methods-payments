<?php

use App\Models\Roles;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolesTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('roles')->delete();
        //Criando os Admins
        $role0 = Roles::create([
            'id'                   => 1,
            'name'                 =>'Administrator',
            'label'                =>'Administrador Master',
            'system'               =>1,
            'group'                =>'Funções do Sistema',
        ]);
        $role0->users()->sync([1]);
    }
}
