<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('users')->delete();
        $user0 = User::create([
            'id'                   =>1,
            'first_name'           =>'Administrador',
            'last_name'            =>'de System',
            'email'                =>'admin@admin',
            'password'             =>bcrypt('cgs@020459'),
            'system'               =>1,
        ]);
    }
}
