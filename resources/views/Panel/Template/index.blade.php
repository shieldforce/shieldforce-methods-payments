<!DOCTYPE html>
<html lang="pt-BR">
    @includeIf("Panel.Template.head")
    <body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
        <div class="wrapper">
            @includeIf("Panel.Template.navbar")
            @includeIf("Panel.Template.sidebar-left")
            @yield('content')
            @includeIf("Panel.Template.footer")
            <!--includeIf("Panel.Template.sidebar-right")-->
            @includeIf("Panel.Template.javascript")
        </div>
    </body>
</html>
