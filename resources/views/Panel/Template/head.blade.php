<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{ $title ?? 'Panel | SFMP' }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{ asset('Auth-Panel/plugins/fontawesome-free/css/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('Auth-Panel/plugins/toastr/toastr.css') }}">
    <link rel="stylesheet" href="{{ asset('Auth-Panel/dist/css/adminlte.min.css') }}">
    <link rel="stylesheet" href="{{ asset('Auth-Panel/plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }}">
    <link href="{{ asset('Auth-Panel/dist/fonts/font1.css') }}" rel="stylesheet">
    <link href="{{ asset('Auth-Panel/dist/css/custom_alexandre.css') }}" rel="stylesheet">
    @if(isset($route[0]) && isset($route[1]) && isset($route[2]))
        @includeIf("$route[0].$route[1].HeadLocal.$route[2]")
    @endif
</head>
