<footer class="main-footer fixed">
    <strong>Copyright &copy; 2014-{{ date('Y') }} <a href="http://adminlte.io">Shieldforce</a>.</strong>
    Todos os direitos reservados.
    <div class="float-right d-none d-sm-inline-block">
        <b>Version</b> 1.0
    </div>
</footer>
