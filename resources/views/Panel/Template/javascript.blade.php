<script src="{{ asset("Auth-Panel/plugins/jquery/jquery.js") }}"></script>
<script src="{{ asset("Auth-Panel/plugins/bootstrap/js/bootstrap.bundle.min.js") }}"></script>
<script src="{{ asset("Auth-Panel/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js") }}"></script>
<script src="{{ asset('Auth-Panel/plugins/toastr/toastr.min.js') }}"></script>
<script src="{{ asset("Auth-Panel/dist/js/adminlte.js") }}"></script>

<script type="text/javascript">
    $(function() {
        @if(session('error'))
        toastr.error("{{ session('error') }}");
        @endif
        @if(session('success'))
        toastr.success("{{ session('success') }}");
        @endif
        @if(session('info'))
        toastr.info("{{ session('info') }}");
        @endif
        @if(session('warning'))
        toastr.warning("{{ session('warning') }}");
        @endif
    });
</script>
@if(isset($route[0]) && isset($route[1]) && isset($route[2]))
    @includeIf("$route[0].$route[1].JavascriptLocal.$route[2]")
@endif
