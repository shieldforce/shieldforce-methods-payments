<div class="col">
    <div class="card card-dark">
        <h5 class="card-header">Criação de {{ $title ?? 'Sem Título' }}</h5>
        <form class="form-horizontal" id="validate-form" method="POST" action="{{ route("$route[0].$route[1].store") }}" enctype="multipart/form-data">

            {{ csrf_field() }}

            <div class="card-body">

                <div class="form-body">
                    <div class="form-group row">
                        <label class="control-label text-right col-md-3">Nome</label>
                        <div class="col-md-5">
                            <input type="text" placeholder="Digite o Primeiro Nome" class="form-control {{ $errors->has('first_name') ? ' is-invalid' : '' }}" name="first_name" autofocus value="{{ old('first_name') ?? "" }}">
                            @if ($errors->has('first_name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('first_name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="control-label text-right col-md-3">Sobrenome</label>
                        <div class="col-md-5">
                            <input type="text" placeholder="Digite o Sobrenome" class="form-control {{ $errors->has('last_name') ? ' is-invalid' : '' }}" name="last_name" autofocus value="{{ old('last_name') ?? "" }}">
                            @if ($errors->has('last_name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('last_name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="control-label text-right col-md-3">E-mail</label>
                        <div class="col-md-5">
                            <input type="email" placeholder="Digite o e-mail" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') ?? "" }}">
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <hr class="dashed ">
                    <div class="form-group row">
                        <label class="control-label text-right col-md-3">Digite a senha</label>
                        <div class="col-md-5">
                            <input type="password" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="Digite a Senha" name="password"  value="{{ old('password') ?? "" }}">
                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <hr class="dashed">
                    <div class="form-group row">
                        <label class="control-label text-right col-md-3">Função</label>
                        <div class="col-md-5">
                            <select class="form-control select2 {{ $errors->has('roles_ids') ? ' is-invalid' : '' }}" name="roles_ids[]" multiple>

                                @foreach($roles as $role)
                                    <option value="{{ $role->id }}">{{ $role->name }}</option>
                                @endforeach

                            </select>
                            @if ($errors->has('roles_ids'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('roles_ids') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer bg-light card-footer-crud fixed">
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="offset-sm-3 col-md-5">
                                    <button type="submit" class="btn btn-dark btn-rounded submit">Cadastrar</button>
                                    <a href="{{ route("$route[0].$route[1].index") }}" class="btn btn-secondary clear-form btn-rounded btn-outline">Cancelar</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
