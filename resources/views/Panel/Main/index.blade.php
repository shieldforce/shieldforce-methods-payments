@extends("Panel.Template.index")

@section('content')
    <div class="content-wrapper">
        @includeIf("Panel.Main.Header.index")
        <section class="content">
            <div class="container-fluid">
                <div class="row">

                    <div class="col-lg-3 col-6">
                        <div class="small-box bg-gradient-dark">
                            <div class="inner">
                                <h3>{{ count(\App\User::where('id', '!=', 1)->get()) }}</h3>
                                <p>Usuários</p>
                            </div>
                            <div class="icon">
                                <i class="fas fa-users"></i>
                            </div>
                            <a href="{{ route("$route[0].Users.index") }}" class="small-box-footer">Acessar <i class="fas fa-arrow-circle-right"></i></a>
                        </div>
                    </div>

                    <div class="col-lg-3 col-6">
                        <div class="small-box bg-gradient-dark">
                            <div class="inner">
                                <h3>{{ count(\App\Models\Roles::where('id', '!=', 1)->get()) }}</h3>
                                <p>Funções</p>
                            </div>
                            <div class="icon">
                                <i class="fas fa-user-check"></i>
                            </div>
                            <a href="{{ route("$route[0].Roles.index") }}" class="small-box-footer">Acessar <i class="fas fa-arrow-circle-right"></i></a>
                        </div>
                    </div>

                    <div class="col-lg-3 col-6">
                        <div class="small-box bg-gradient-dark">
                            <div class="inner">
                                <h3>{{ count(\App\Models\Permissions::all()) }}</h3>
                                <p>Permissões</p>
                            </div>
                            <div class="icon">
                                <i class="fas fa-user-lock"></i>
                            </div>
                            <a href="{{ route("$route[0].Permissions.index") }}" class="small-box-footer">Acessar <i class="fas fa-arrow-circle-right"></i></a>
                        </div>
                    </div>

                    <div class="col-lg-3 col-6">
                        <div class="small-box bg-gradient-dark">
                            <div class="inner">
                                <h3>0</h3>

                                <p>Vago</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-pie-graph"></i>
                            </div>
                            <a href="{{ route("$route[0].$route[1].index") }}" class="small-box-footer">Acessar <i class="fas fa-arrow-circle-right"></i></a>
                        </div>
                    </div>

                </div>
            </div>
        </section>
    </div>
@endsection
