<div class="col">
    <div class="card card-dark">
        <h5 class="card-header">Edição de {{ $title ?? 'Sem Título' }}</h5>
        <form class="form-horizontal" id="validate-form" method="POST" action="{{ route("$route[0].$route[1].update") }}" enctype="multipart/form-data">

            {{ csrf_field() }}

            <input type="hidden" name="id" value="{{ $model->id ?? null }}">

            <div class="card-body">
                <div class="form-body">
                    <div class="form-group row">
                        <label class="control-label text-right col-md-3">Nome</label>
                        <div class="col-md-5">
                            <input type="text" placeholder="Nome da Função" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" autofocus name="name" value="{{ old('name') ? old('name') : $model->name }}">
                            @if ($errors->has('name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <hr class="dashed ">
                    <div class="form-group row">
                        <label class="control-label text-right col-md-3">Descrição</label>
                        <div class="col-md-5">
                            <textarea placeholder="Digite a descrição" class="form-control {{ $errors->has('label') ? ' is-invalid' : '' }}" name="label" rows="5">{{ old('label') ? old('label') : $model->label }}</textarea>
                            @if ($errors->has('label'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('label') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <hr class="dashed ">
                    <div class="form-group row">
                        <label class="control-label text-right col-md-3">Grupo</label>
                        <div class="col-md-5">
                            <input type="text" placeholder="Nome do Grupo" class="form-control {{ $errors->has('group') ? ' is-invalid' : '' }}" name="group" value="{{ old('group') ? old('group') : $model->group }}">
                            @if ($errors->has('group'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('group') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <hr class="dashed ">
                    <div class="form-group row">
                        <label class="control-label text-right col-md-3">Funções: </label>
                        <div class="col-md-5">
                            <input type="hidden" name="groups" value="ok">
                            <select class="form-control duallistbox {{ $errors->has('roles_ids') ? ' is-invalid' : '' }}" multiple="multiple" name="roles_ids[]" style="height: 300px;">
                                @foreach($model->roles as $roleT)
                                    @if($roleT->group!='Levels')
                                        <option value="{{ $roleT->id }}" selected>{{ $roleT->name }} --- ({{ $roleT->label }})</option>
                                    @endif
                                @endforeach
                                @foreach($groups as $group)
                                    @if($group->group!='Levels')
                                        <optgroup label="{{ $group->group }}">
                                            @foreach($roles as $role)
                                                @if($role->group==$group->group && $role->group!='Levels')
                                                    @if($role->id!=1)
                                                        <option value="{{ $role->id }}">{{ $role->name }} --- ({{ $role->label }})</option>
                                                    @endif
                                                @endif
                                            @endforeach
                                        </optgroup>
                                    @endif
                                @endforeach
                            </select>
                            @if ($errors->has('roles_ids'))
                                <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('roles_ids') }}</strong>
                        </span>
                            @endif
                        </div>
                    </div>
                    <hr class="dashed ">
                </div>
            </div>
            <div class="card-footer bg-light">
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="offset-sm-3 col-md-5">
                                    <button type="submit" class="btn btn-dark btn-rounded submit">Atualizar</button>
                                    <a href="{{ route("$route[0].$route[1].index") }}" class="btn btn-secondary clear-form btn-rounded btn-outline">Cancelar</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
