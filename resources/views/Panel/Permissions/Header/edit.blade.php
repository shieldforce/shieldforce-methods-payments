<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Edição de {{ $crud ?? 'Não Existe Crud Setado' }}</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ route("$route[0].$route[1].index") }}">{{ $crud ?? 'Não Informado' }}</a></li>
                    <li class="breadcrumb-item active">Edição de {{ $crud ?? 'Não Existe Crud Setado' }}</li>
                </ol>
            </div>
        </div>
    </div>
</div>
