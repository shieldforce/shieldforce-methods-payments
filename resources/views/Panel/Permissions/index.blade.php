@extends("$route[0].Template.index")

@section('content')
    <div class="content-wrapper">

        @includeIf("$route[0].$route[1].Header.$route[2]")

        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    @includeIf("$route[0].$route[1].Contents.$route[2]")
                </div>
            </div>
        </section>

    </div>
@endsection
