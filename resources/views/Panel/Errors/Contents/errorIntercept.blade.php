<section class="content">
    <div class="error-page">
        <h2 class="headline text-danger">
            <img src="{{ asset(isset($imgError) ? $imgError : "Auth-Panel/images/error.png") }}" width="150" height="150">
        </h2>

        <div class="error-content">
            <h3><i class="fas fa-exclamation-triangle text-danger"></i> {{ isset($titleError) ? $titleError : 'Oops! Algo deu errado.' }}</h3>
            <p>
                {{ isset($textError) ? $textError : 'Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing industries for previewing layouts and visual mockups.' }}
            </p>

            <p>
                - {{ isset($lineError) ? $lineError : '' }}
            </p>

            <p>
                - {{ isset($fileError) ? $fileError : '' }}
            </p>

            @if(isset($action) && $action=='button')

                <a class="btn btn-primary" href="{{ route("Panel.Main.index") }}">Página Principal</a>

            @elseif(isset($action) && $action=='search')

                <form class="search-form">
                    <div class="input-group">
                        <input type="text" name="message" class="form-control" placeholder="Informar Error ...">

                        <div class="input-group-append">
                            <button type="submit" name="submit" class="btn btn-danger"><i class="fas fa-envelope"></i>
                            </button>
                        </div>
                    </div>
                </form>

            @endif

        </div>
    </div>
</section>
