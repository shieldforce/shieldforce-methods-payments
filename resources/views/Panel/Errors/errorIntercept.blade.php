<!DOCTYPE html>
<html lang="pt-BR">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>{{ "Panel | ".$titleError ?? 'Panel | Error' }}</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="{{ asset('Auth-Panel/plugins/fontawesome-free/css/all.min.css') }}">
        <link rel="stylesheet" href="{{ asset('Auth-Panel/plugins/toastr/toastr.css') }}">
        <link rel="stylesheet" href="{{ asset('Auth-Panel/dist/css/adminlte.min.css') }}">
        <link rel="stylesheet" href="{{ asset('Auth-Panel/plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }}">
        <link href="{{ asset('Auth-Panel/dist/fonts/font1.css') }}" rel="stylesheet">
    </head>
    <body class="hold-transition sidebar-mini">
        <div class="wrapper">
            <nav class="main-header navbar navbar-expand navbar-white navbar-light">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" data-widget="pushmenu" href="{{ asset('Auth-Panel') }}/#"><i class="fas fa-bars"></i></a>
                    </li>
                    <li class="nav-item d-none d-sm-inline-block">
                        <a href="{{ route("Panel.Main.index") }}" class="nav-link">Home</a>
                    </li>
                </ul>
            </nav>
            <aside class="main-sidebar sidebar-dark-primary elevation-4">
                <a href="{{ route("Panel.Main.index") }}" class="brand-link">
                    <img src="{{ asset("Auth-Panel/dist/img/logo2a.png") }}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
                         style="opacity: .8">
                    <span class="brand-text font-weight-light">{{ env('APP_NAME') }}</span>
                </a>
                <div class="sidebar">
                    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                        <div class="image">
                            <img src="{{ asset('Auth-Panel/dist/img/user2-160x160.jpg') }}" class="img-circle elevation-2" alt="User Image">
                        </div>
                        <div class="info">
                            <a href="{{ asset('Auth-Panel') }}/#" class="d-block">User System</a>
                        </div>
                    </div>
                    <nav class="mt-2"></nav>
                </div>
            </aside>
            <div class="content-wrapper">
                <section class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-sm-6">
                                <h1>{{ "Error Code | ".$codeError ?? 'Error Code | 000' }}</h1>
                            </div>
                            <div class="col-sm-6">
                                <ol class="breadcrumb float-sm-right">
                                    <li class="breadcrumb-item"><a href="{{ route("Panel.Main.index") }}">Home</a></li>
                                    <li class="breadcrumb-item active">{{ $titleError ?? 'Not Title' }}</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="content">
                    <div class="container">
                        <h2 class="headline text-danger">
                            <img src="{{ asset(isset($imgError) ? $imgError : "Auth-Panel/images/error.png") }}" width="150" height="150">
                        </h2>
                        <div class="error-content">
                            <h3><i class="fas fa-exclamation-triangle text-danger"></i> {{ $titleError ?? 'Not Title' }}</h3>
                            <textarea class="col-12" style="min-height: 150px;border-radius: 5px;" disabled>
                                {{ $textError ?? 'Not Text' }}
                            </textarea>

                            <ul class="col-12">
                                <li>Arquivo: {{ isset($fileError) ? $fileError : '' }}</li>
                                <li>Linha: {{ isset($lineError) ? $lineError : '' }}</li>
                            </ul>

                            @if(isset($codeError) && $codeError==401)
                                <a class="btn btn-success" href="{{ route("Panel.Main.index") }}">Perdir Permissão</a>
                            @endif

                            @if(isset($action) && $action=='button')
                                <a class="btn btn-danger" href="{{ route("Panel.Main.index") }}">Página Principal</a>
                            @elseif(isset($action) && $action=='search')
                                <form class="search-form">
                                    <div class="input-group">
                                        <input type="text" name="message" class="form-control" placeholder="Informar Error ...">

                                        <div class="input-group-append">
                                            <button type="submit" name="submit" class="btn btn-danger"><i class="fas fa-envelope"></i>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            @endif
                        </div>
                    </div>
                </section>
            </div>
            <footer class="main-footer fixed">
                <strong>Copyright &copy; 2014-{{ date('Y') }} <a href="http://adminlte.io">Shieldforce</a>.</strong>
                Todos os direitos reservados.
                <div class="float-right d-none d-sm-inline-block">
                    <b>Version</b> 1.0
                </div>
            </footer>
        </div>
    <script src="{{ asset("Auth-Panel/plugins/jquery/jquery.js") }}"></script>
    <script src="{{ asset("Auth-Panel/plugins/bootstrap/js/bootstrap.bundle.min.js") }}"></script>
    <script src="{{ asset("Auth-Panel/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js") }}"></script>
    <script src="{{ asset('Auth-Panel/plugins/toastr/toastr.min.js') }}"></script>
    <script src="{{ asset("Auth-Panel/dist/js/adminlte.js") }}"></script>
    <script type="text/javascript">
        $(function() {
            @if(session('error'))
            toastr.error("{{ session('error') }}");
            @endif
            @if(session('success'))
            toastr.success("{{ session('success') }}");
            @endif
            @if(session('info'))
            toastr.info("{{ session('info') }}");
            @endif
            @if(session('warning'))
            toastr.warning("{{ session('warning') }}");
            @endif
        });
    </script>
    </body>
</html>

