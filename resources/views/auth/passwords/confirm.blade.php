@extends("auth.Template.index")

@section('content')

    <div class="login-box">
        <div class="login-logo">
            <img src="{{ asset('Auth-Panel/images/logo1.png') }}" width="150px" height="150px">
        </div>
        <div class="card">
            <div class="card-body login-card-body">
                <p class="login-box-msg">Confirmar Senha!</p>

                <form action="{{ route('password.confirm') }}" method="post">

                    {{ csrf_field() }}

                    <div class="input-group mb-3">
                        <input type="password" class="form-control @error('password') is-invalid @enderror" placeholder="Senha" name="password" value="{{ old('password') ?? '' }}">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div>
                        </div>
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="input-group mb-3">
                        <input type="password" class="form-control" placeholder="Repita a Senha"  name="password_confirmation" value="{{ old('password_confirmation') ?? '' }}">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div>
                        </div>
                        @error('password_confirmation')
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="row">
                        <div class="col-12">
                            <button type="submit" class="btn btn-dark btn-block">Confirmar</button>
                        </div>
                    </div>

                </form>

                <div class="social-auth-links text-center mb-3">
                    <hr>
                    <a href="{{ route("register") }}" class="btn btn-block btn-default">
                        <i class="fa fa-user-plus"></i> |  Criar uma conta!
                    </a>
                    <a href="{{ route('password.request') }}" class="btn btn-block btn-default">
                        <i class="fa fa-lock"></i> | Recuperar a Senha
                    </a>
                </div>
            </div>
        </div>
    </div>

@endsection
