@extends("auth.Template.index")

@section('content')

    <div class="login-box">
        <div class="login-logo">
            <img src="{{ asset('Auth-Panel/images/logo1.png') }}" width="150px" height="150px">
        </div>
        <div class="card">
            <div class="card-body login-card-body">
                <p class="login-box-msg">Escreva seu e-mail e envie.</p>

                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif

                <form action="{{ route("password.email") }}" method="post">

                    {{ csrf_field() }}

                    <div class="input-group mb-3">
                        <input type="email" class="form-control @error('email') is-invalid @enderror" placeholder="E-mail" autocomplete="email" name="email" value="{{ old('email') ?? '' }}">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-envelope"></span>
                            </div>
                        </div>
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="row">
                        <div class="col-12">
                            <button type="submit" class="btn btn-dark btn-block">Enviar</button>
                        </div>
                    </div>

                </form>

                <div class="social-auth-links text-center mb-3">
                    <hr>
                    <a href="{{ route("login") }}" class="btn btn-block btn-default">
                        <i class="fa fa-user-check"></i> |  Entre com sua conta!
                    </a>
                </div>
            </div>
        </div>
    </div>

@endsection


