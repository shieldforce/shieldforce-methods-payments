@extends("auth.Template.index")

@section('content')

    <div class="login-box">
        <div class="login-logo">
            <img src="{{ asset('Auth-Panel/images/logo1.png') }}" width="150px" height="150px">
        </div>
        <div class="card">
            <div class="card-body login-card-body">
                <p class="login-box-msg">Reset de | Senha!</p>

                @if (session('resent'))
                    <div class="alert alert-success" role="alert">
                        {{ __('Um novo link de verificação foi enviado para o seu endereço de e-mail.') }}
                    </div>
                @endif

                <form action="{{ route('verification.resend') }}" method="post">
                    {{ csrf_field() }}

                    <div class="row">
                        <div class="col-12">
                            <button type="submit" class="btn btn-dark btn-block">Enviar</button>
                        </div>
                    </div>
                </form>

                <div class="social-auth-links text-center mb-3">
                    <hr>
                    <a href="{{ route("register") }}" class="btn btn-block btn-default">
                        <i class="fa fa-user-plus"></i> |  Criar uma conta!
                    </a>
                    <a href="{{ route("login") }}" class="btn btn-block btn-default">
                        <i class="fa fa-user-check"></i> |  Entre com sua conta!
                    </a>
                </div>
            </div>
        </div>
    </div>

@endsection
