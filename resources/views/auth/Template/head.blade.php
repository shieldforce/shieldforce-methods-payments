<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{ $title ?? 'Acessos | SFMP' }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{ asset('Auth-Panel/plugins/fontawesome-free/css/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('Auth-Panel/plugins/toastr/toastr.css') }}">
    <link rel="stylesheet" href="{{ asset('Auth-Panel/dist/css/ionicons/2.0.1/ionicons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('Auth-Panel/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('Auth-Panel/dist/css/adminlte.min.css') }}">
    <link href="{{ asset('Auth-Panel/dist/fonts/font1.css') }}" rel="stylesheet">
    <style>
        .login-box{
            background-color: rgba(255,255,255,0.3);
        }
        .login-page{
            background-image: url({{ asset("Auth-Panel/images/background-login.jpg") }});
            background-repeat: no-repeat;
            background-position: center center;
            background-attachment: fixed;
            background-size: 100% 100vh;
        }
    </style>

</head>
