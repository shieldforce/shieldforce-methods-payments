<!DOCTYPE html>
<html lang="pt-BR">
    @includeIf("auth.Template.head")
    <body class="hold-transition login-page">
        @yield('content')
        @includeIf("auth.Template.javascript")
    </body>
</html>

