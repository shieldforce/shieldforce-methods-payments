@extends("auth.Template.index")

@section('content')

    <div class="login-box">
        <div class="login-logo">
            <img src="{{ asset('Auth-Panel/images/logo1.png') }}" width="150px" height="150px">
        </div>
        <div class="card">
            <div class="card-body login-card-body">
                <p class="login-box-msg">Crie seu usuário.</p>

                <form action="{{ route("register") }}" method="post">

                    {{ csrf_field() }}

                    <div class="input-group mb-3">
                        <input type="text" class="form-control @error('first_name') is-invalid @enderror" placeholder="Nome" autocomplete="first_name" autofocus name="first_name" value="{{ old('first_name') ?? '' }}">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-user-check"></span>
                            </div>
                        </div>
                        @error('first_name')
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="input-group mb-3">
                        <input type="text" class="form-control @error('last_name') is-invalid @enderror" placeholder="Sobrenome" autocomplete="last_name" name="last_name" value="{{ old('last_name') ?? '' }}">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-user-check"></span>
                            </div>
                        </div>
                        @error('last_name')
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="input-group mb-3">
                        <input type="email" class="form-control @error('email') is-invalid @enderror" placeholder="E-mail" autocomplete="email" name="email" value="{{ old('email') ?? '' }}">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-envelope"></span>
                            </div>
                        </div>
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="input-group mb-3">
                        <input type="password" class="form-control @error('password') is-invalid @enderror" placeholder="Senha" name="password" value="{{ old('password') ?? '' }}">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div>
                        </div>
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="input-group mb-3">
                        <input type="password" class="form-control" placeholder="Repita a Senha"  name="password_confirmation" value="{{ old('password_confirmation') ?? '' }}">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div>
                        </div>
                        @error('password_confirmation')
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="row">
                        <div class="col-12">
                            <button type="submit" class="btn btn-dark btn-block">Cadastrar</button>
                        </div>
                    </div>

                </form>

                <div class="social-auth-links text-center mb-3">
                    <hr>
                    <a href="{{ route("login") }}" class="btn btn-block btn-default">
                        <i class="fa fa-user-check"></i> |  Entre com sua conta!
                    </a>
                </div>
            </div>
        </div>
    </div>

@endsection
