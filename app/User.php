<?php

namespace App;

use App\Models\Permissions;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'first_name', 'last_name', 'email', 'password',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /*
     *
     * Relacionamentos
     *
     */
    public function roles()
    {
        return $this->belongsToMany(\App\Models\Roles::class, 'role_user', 'user_id', 'role_id');
    }

    /*
     *
     * ACL -----------
     *
     */
    public function hasPermission(Permissions $permission)
    {
        return $this->hasAnyRoles($permission->roles);
    }
    public function hasAnyRoles($roles)
    {
        if( is_array($roles) || is_object($roles) ) {
            return !! $roles->intersect($this->roles)->count();
        }
        return $this->roles->contains('name', $roles);
    }

    // Método para configurar o ShieldForce Organizer ------------------------------------------------------------------
    public function configController()
    {
        return
            [
                'variables'                           =>
                    [
                        'crudName'                    =>'Usuários',
                    ],
                'store'                               =>
                    [
                        'first_name'                  => ['required', 'string', 'max:30'],
                        'last_name'                   => ['required', 'string', 'max:50'],
                        'email'                       => ['required', 'string', 'email', 'max:255', 'unique:users'],
                        'password'                    => ['required', 'string', 'min:8'],
                        'roles_ids'                   => ['required']
                    ],
                'update'                              =>
                    [
                        'first_name'                  => ['required', 'string', 'max:30'],
                        'last_name'                   => ['required', 'string', 'max:50'],
                        'email'                       => ['required', 'string', 'email', 'max:255'],
                        'password'                    => ['required', 'string', 'min:8'],
                        'roles_ids'                   => ['required']
                    ],
                'delete'                              =>
                    [
                        'id'                          => ['required'],
                    ],
                'messages'                            =>
                    [
                        //'first_name.required'     => 'Este campo é obrigatório!',
                        //'last_name.required'      => 'Este campo é obrigatório!',
                        //'email.required'          => 'Este campo é obrigatório!',
                        //'password.required'       => 'Este campo é obrigatório!',
                    ],
            ];
    }
    //------------------------------------------------------------------------------------------------------------------

}
