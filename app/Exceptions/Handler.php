<?php

namespace App\Exceptions;

use Exception;
use Facade\Ignition\Exceptions\ViewException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\UnauthorizedException;
use Illuminate\Validation\ValidationException;
use Symfony\Component\Debug\Exception\FatalThrowableError;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, \Exception $exception)
    {

        if(env('APP_DEBUG')==false)
        {
            if( $exception instanceof NotFoundHttpException )
            {
                $crud           = 'Página não Encontrada!';
                $codeError      = 404;
                $lineError      = "";
                $fileError      = "";
                $imgError       = "Auth-Panel/images/error.png";
                $titleError     = 'Está página não existe!';
                $textError      = "Você está tentando acessar um endereço que não existe. Por favor, acesse um endereço existente ou Retorne para Página Principal, Clicando no Botão Página Principal!";
                $action         = 'button';
                return response()->view('Panel.Errors.errorIntercept', compact('crud', 'titleError', 'codeError', 'imgError', 'textError', 'action', 'lineError', 'fileError'));
            }
            elseif( $exception instanceof  ViewException )
            {
                $crud           = 'Error de View!';
                $codeError      = $exception->getCode();
                $lineError      = $exception->getLine();
                $fileError      = $exception->getFile();
                $imgError       = "Auth-Panel/images/error.png";
                $titleError     = 'Error de View!';
                $textError      = $exception->getMessage();
                $action         = 'search';
                return response()->view('Panel.Errors.errorIntercept', compact('crud', 'titleError', 'codeError', 'imgError', 'textError', 'action', 'lineError', 'fileError'));
            }
            elseif( $exception instanceof FatalThrowableError )
            {
                $crud           = 'Erro de FatalThrowableError !';
                $codeError      = $exception->getCode();
                $lineError      = $exception->getLine();
                $fileError      = $exception->getFile();
                $imgError       = "Auth-Panel/images/error.png";
                $titleError     = 'Erro de FatalThrowableError !';
                $textError      = $exception->getMessage();
                $action         = 'search';
                return response()->view('Panel.Errors.errorIntercept', compact('crud', 'titleError', 'codeError', 'imgError', 'textError', 'action', 'lineError', 'fileError'));
            }
            elseif( $exception instanceof \RuntimeException )
            {
                $crud           = 'Error de Runtime!';
                $codeError      = $exception->getCode();
                $lineError      = $exception->getLine();
                $fileError      = $exception->getFile();
                $imgError       = "Auth-Panel/images/error.png";
                $titleError     = 'Error de Runtime!';
                $textError      = $exception->getMessage();
                $action         = 'search';
                return response()->view('Panel.Errors.errorIntercept', compact('crud', 'titleError', 'codeError', 'imgError', 'textError', 'action', 'lineError', 'fileError'));
            }
            elseif( $exception instanceof ModelNotFoundException )
            {
                $crud           = 'Error de ModelNotFoundException!';
                $codeError      = $exception->getCode();
                $lineError      = $exception->getLine();
                $fileError      = $exception->getFile();
                $imgError       = "Auth-Panel/images/error.png";
                $titleError     = 'Error de ModelNotFoundException!';
                $textError      = $exception->getMessage();
                $action         = 'search';
                return response()->view('Panel.Errors.errorIntercept', compact('crud', 'titleError', 'codeError', 'imgError', 'textError', 'action', 'lineError', 'fileError'));
            }
            elseif( $exception instanceof QueryException )
            {
                $crud           = 'Error de QueryException!';
                $codeError      = $exception->getCode();
                $lineError      = $exception->getLine();
                $fileError      = $exception->getFile();
                $imgError       = "Auth-Panel/images/error.png";
                $titleError     = 'Error de QueryException!';
                $textError      = $exception->getMessage();
                $action         = 'search';
                return response()->view('Panel.Errors.errorIntercept', compact('crud', 'titleError', 'codeError', 'imgError', 'textError', 'action', 'lineError', 'fileError'));
            }
            elseif( $exception instanceof UnauthorizedException )
            {
                $crud           = 'Error de UnauthorizedException!';
                $codeError      = $exception->getCode();
                $lineError      = $exception->getLine();
                $fileError      = $exception->getFile();
                $imgError       = "Auth-Panel/images/error.png";
                $titleError     = 'Error de UnauthorizedException!';
                $textError      = $exception->getMessage();
                $action         = 'search';
                return response()->view('Panel.Errors.errorIntercept', compact('crud', 'titleError', 'codeError', 'imgError', 'textError', 'action', 'lineError', 'fileError'));
            }
            elseif( $exception instanceof \InvalidArgumentException )
            {
                $crud           = 'Error de InvalidArgumentException!';
                $codeError      = $exception->getCode();
                $lineError      = $exception->getLine();
                $fileError      = $exception->getFile();
                $imgError       = "Auth-Panel/images/error.png";
                $titleError     = 'Error de InvalidArgumentException!';
                $textError      = $exception->getMessage();
                $action         = 'button';
                return response()->view('Panel.Errors.errorIntercept', compact('crud', 'titleError', 'codeError', 'imgError', 'textError', 'action', 'lineError', 'fileError'));
            }
            elseif( $exception instanceof \ErrorException )
            {
                $crud           = 'Error de ErrorException!';
                $codeError      = $exception->getCode();
                $lineError      = $exception->getLine();
                $fileError      = $exception->getFile();
                $imgError       = "Auth-Panel/images/error.png";
                $titleError     = 'Error de ErrorException!';
                $textError      = $exception->getMessage();
                $action         = 'button';
                return response()->view('Panel.Errors.errorIntercept', compact('crud', 'titleError', 'codeError', 'imgError', 'textError', 'action', 'lineError', 'fileError'));
            }
            elseif( $exception instanceof AuthenticationException )
            {
                return back()->with('error', 'Acesso negado')->withErrors('error', 'Acesso negado');
            }

        }

        return parent::render($request, $exception);
    }
}
