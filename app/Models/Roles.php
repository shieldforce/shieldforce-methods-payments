<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Roles extends Model
{

    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $table = 'roles';

    protected $fillable =
        [
            'name',
            'label',
            'system',
            'group'
        ];

    /*
     *
     * Relacionamentos
     *
     */
    public function users()
    {
        return $this->belongsToMany(\App\User::class, 'role_user', 'role_id', 'user_id');
    }

    public function permissions()
    {
        return $this->belongsToMany(\App\Models\Permissions::class, 'permission_role', 'role_id', 'permission_id');
    }


    // Método para configurar o ShieldForce Organizer ------------------------------------------------------------------
    public function configController()
    {
        return
            [
                'variables'                   =>
                    [
                        'crudName'            =>'Funções',
                    ],
                'store'                       =>
                    [
                        'name'                => ['required'],
                        'label'               => ['required'],
                        'group'               => ['required']
                    ],
                'update'                      =>
                    [
                        'id'                  => ['required'],
                        'name'                => ['required'],
                        'label'               => ['required'],
                        'group'               => ['required']
                    ],
                'delete'                      =>
                    [
                        'id'                  => ['required'],
                    ],
                'messages'                    =>
                    [
                        //'name.required'     => 'Este campo é obrigatório!',
                        //'label.required'    => 'Este campo é obrigatório!',
                        //'group.required'    => 'Este campo é obrigatório!',
                    ],
            ];
    }
    //------------------------------------------------------------------------------------------------------------------
}


