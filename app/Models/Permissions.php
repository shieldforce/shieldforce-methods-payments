<?php

    namespace App\Models;

    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Database\Eloquent\SoftDeletes;

    class Permissions extends Model
    {

        use SoftDeletes;

        protected $dates = ['deleted_at'];

        protected $table = 'permissions';

        protected $fillable =
            [
                'name',
                'label',
                'system',
                'group',
                'default'
            ];

        /*
         *
         * Relacionamentos
         *
         */
        public function roles()
        {
            return $this->belongsToMany(\App\Models\Roles::class, 'permission_role', 'permission_id', 'role_id');
        }


        // Método para configurar o ShieldForce Organizer ------------------------------------------------------------------
        public function configController()
        {
            return
                [
                    'variables'                   =>
                        [
                            'crudName'            =>'Permissões',
                        ],
                    'store'                       =>
                        [
                            'name'                => ['required'],
                            'label'               => ['required'],
                            'group'               => ['required']
                        ],
                    'update'                      =>
                        [
                            'id'                  => ['required'],
                            'name'                => ['required'],
                            'label'               => ['required'],
                            'group'               => ['required']
                        ],
                    'delete'                      =>
                        [
                            'id'                  => ['required'],
                        ],
                    'messages'                    =>
                        [
                            //'name.required'     => 'Este campo é obrigatório!',
                            //'label.required'    => 'Este campo é obrigatório!',
                            //'group.required'    => 'Este campo é obrigatório!',
                        ],
                ];
        }
        //------------------------------------------------------------------------------------------------------------------

    }

