<?php

namespace App\Models;


class MainPanel
{

    public static function all(){return true;}
    public static function find(){return true;}
    public static function create(){return true;}
    public static function update(){return true;}
    public static function delete(){return true;}

    //-----------------------------------------------------------------------------
    public function configController()
    {
        return
            [
                'variables'                           =>
                    [
                        'crudName'                    =>'Painel de Controle',
                    ],
                'store'                               =>
                    [

                    ],
                'update'                              =>
                    [

                    ],
                'delete'                              =>
                    [

                    ],
                'messages'                            =>
                    [

                    ],
            ];
    }
    //---------------------------------------------------------------------------
}
