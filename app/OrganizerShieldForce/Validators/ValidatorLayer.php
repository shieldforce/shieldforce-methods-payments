<?php

    namespace App\OrganizerShieldforce\Validators;

    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Validator;

    class ValidatorLayer
    {
        protected $request;

        public function __construct(Request $request)
        {
            $this->request = $request;
        }

        public function return(Request $request)
        {
            if(isset($request['validations']) ? $validations = $request['validations'] : $validations = []);
            if(isset($request['messages']) ? $messages = $request['messages'] : $messages = []);
            $validator = Validator::make($request->all(), $validations, $messages);
            return $validator;
        }
    }
