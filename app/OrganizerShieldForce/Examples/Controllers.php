<?php
    /*
     *
     *
     * Método paraa testes
     *

    public function test(Request $request)
    {
        //---------------------------------------------------------------------------------
        //Inserir qualquer dado na view ou retorno do compact -----------------------------
        $this->setCompactController([
            'crud'                 => "Painel Principal",
            'title'                => env("APP_NAME")." | Panel de Controle",
        ]);
        //---------------------------------------------------------------------------------
        //Os campos a serem validados no request ------------------------------------------
        $this->validationsKeys($request, [
            'campo1'               => ['required'],
            'campo2'               => ['string',],
            'campo3'               => ['max:5'],
            'campo4'               => ['min:5'],
            'campo5'               => ['date'],
        ]);
        //---------------------------------------------------------------------------------
        //Substituição das mensagens padrões das validações -------------------------------
        $this->validationsMessages($request, [
            'campo1.required'      => 'Este campo é obrigatório!',
            'campo2.string'        => 'Este campo aceita somente texto!',
            'campo3.max'           => 'Este campo aceita no máximo 5 letras!',
            'campo4.min'           => 'Este campo aceita no mínimo 5 letras',
            'campo5.date'          => 'Este campo só aceita o formato de data!',
        ]);
        //---------------------------------------------------------------------------------
        //Alguns exemplos de substituição ou simulação de requests ------------------------
        $this->modifyRequest($request, [
            $request['campo1']     = null,
            $request['campo2']     = 123456789,
            $request['campo3']     = '123456789',
            $request['campo4']     = '123',
            $request['campo5']     = 'abcdef'
        ]);
        //---------------------------------------------------------------------------------
        // Chamando a classe de service ---------------------------------------------------
        $service   = $this->service->return($request)->getData();
        //---------------------------------------------------------------------------------
        //---------------------------------------------------------------------------------

        $this->view($request, 'User', compact(''));
        $this->show($request, 'User', 2);
        $this->list($request, 'User');


        $this->modifyRequest($request, [
            $request['campo1']     = null,
            $request['campo2']     = 123456789,
            $request['campo3']     = '123456789',
            $request['campo4']     = '123',
            $request['campo5']     = 'abcdef'
        ]);
        $this->store($request, 'User');



        $this->modifyRequest($request, [
            $request['campo1']     = null,
            $request['campo2']     = 123456789,
            $request['campo3']     = '123456789',
            $request['campo4']     = '123',
            $request['campo5']     = 'abcdef'
        ]);
        $this->update($request, 'User', 2);



        $this->delete($request, 'User', 2);

        //dd($service);
    }
