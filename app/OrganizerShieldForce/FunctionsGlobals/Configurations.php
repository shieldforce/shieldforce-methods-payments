<?php


    namespace App\OrganizerShieldForce\FunctionsGlobals;


    use Illuminate\Http\Request;
    use Illuminate\Routing\Router;

    class Configurations
    {

        protected $request;
        protected $router;

        public function __construct(Request $request, Router $router)
        {
            $this->request = $request;
            $this->router = $router;
        }

        public function routeList()
        {
            return array_keys($this->router->getRoutes()->getRoutesByName());
        }

        public function rotaFull()
        {
            return explode('/', $this->request->getUri());
        }

        public function ipClient()
        {
            $ipaddress = '';
            if (isset($_SERVER['HTTP_CLIENT_IP']))
                $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
            else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
                $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
            else if(isset($_SERVER['HTTP_X_FORWARDED']))
                $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
            else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
                $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
            else if(isset($_SERVER['HTTP_FORWARDED']))
                $ipaddress = $_SERVER['HTTP_FORWARDED'];
            else if(isset($_SERVER['REMOTE_ADDR']))
                $ipaddress = $_SERVER['REMOTE_ADDR'];
            else
                $ipaddress = 'UNKNOWN';
            return $ipaddress;
        }

        public function getRouteName()
        {
            if($this->request->route()!=null)
            {
                return explode('.', $this->request->route()->getName());
            }
            else
            {
                return 'Home.Main.index';
            }
        }

        public function getMethod()
        {
            if($this->getRouteName()!=null)
            {
                $method = isset($this->getRouteName()[2]) ? $this->getRouteName()[2] : 'index';
                return $method;
            }
            else
            {
                return 'index';
            }
        }

        public function Tagear($string)
        {
            //Transformando o titulo em tag
            $urlP = $string;
            $table = array(
                'Š'=>'S', 'š'=>'s', 'Đ'=>'Dj', 'đ'=>'dj', 'Ž'=>'Z',
                'ž'=>'z', 'Č'=>'C', 'č'=>'c', 'Ć'=>'C', 'ć'=>'c',
                'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A',
                'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E',
                'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I',
                'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O',
                'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U', 'Ú'=>'U',
                'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss',
                'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a',
                'å'=>'a', 'æ'=>'a', 'ç'=>'c', 'è'=>'e', 'é'=>'e',
                'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i',
                'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o',
                'ô'=>'o', 'õ'=>'o', 'ö'=>'o', 'ø'=>'o', 'ù'=>'u',
                'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'ý'=>'y', 'þ'=>'b',
                'ÿ'=>'y', 'Ŕ'=>'R', 'ŕ'=>'r',
            );
            // Traduz os caracteres em $string, baseado no vetor $table
            $urlP = strtr($urlP, $table);
            // converte para minúsculo
            $urlP = strtolower($urlP);
            // remove caracteres indesejáveis (que não estão no padrão)
            $urlP = preg_replace("/[^a-z0-9_\s-]/", "", $urlP);
            // Remove múltiplas ocorrências de hífens ou espaços
            $urlP = preg_replace("/[\s-]+/", " ", $urlP);
            // Transforma espaços e underscores em hífens
            $urlP = preg_replace("/[\s_]/", "-", $urlP);
            //Transformando o titulo em tag
            return $urlP;
        }

        public function SalvarImagem($request, $inputName, $caminho, $largura, $altura)
        {
            if($request->hasFile($inputName)){
                $arquivo = $request->file($inputName);
                $filename = time() . '-' . rand() . '.' . $arquivo->getClientOriginalExtension();
                if($arquivo->getClientOriginalExtension() != 'jpg' && $arquivo->getClientOriginalExtension() != 'jpeg' && $arquivo->getClientOriginalExtension() != 'png' && $arquivo->getClientOriginalExtension() != 'PNG' && $arquivo->getClientOriginalExtension() != 'JPG'){
                    return false;
                }else{
                    Image::make($arquivo)->resize($largura, $altura)->save($caminho.''.$filename);
                    return $filename;
                }
            }
            else{
                return false;
            }
        }

        public function EditarImagem($request, $inputName, $inputNamebanco, $caminho, $largura, $altura)
        {
            if($request->hasFile($inputName)){
                $arquivo = $request->file($inputName);
                $filename = time() . '-' . rand() . '.' . $arquivo->getClientOriginalExtension();
                if($arquivo->getClientOriginalExtension() != 'jpg' && $arquivo->getClientOriginalExtension() != 'jpeg' && $arquivo->getClientOriginalExtension() != 'png' && $arquivo->getClientOriginalExtension() != 'PNG' && $arquivo->getClientOriginalExtension() != 'JPG'){
                    return $request->$inputNamebanco;
                }else{
                    //Excluindo arquivo de imagem se ele existir
                    if(file_exists($caminho.''.$request->$inputNamebanco) && $request->$inputNamebanco!='')
                    {
                        unlink($caminho.''.$request->$inputNamebanco."");
                    }
                    Image::make($arquivo)->resize($largura, $altura)->save($caminho.''.$filename);
                    return $filename;
                }
            }
            else{
                return $request->$inputNamebanco;
            }
        }

        public function ExcluirImagem($nome, $caminho)
        {
            //Excluindo arquivo de imagem se ele existir
            if(file_exists($caminho.''.$nome) && $nome!='')
            {
                unlink($caminho.''.$nome."");
            }
            return true;
        }

        public function CreateUniqueFile($request, $path, $inputName)
        {
            $nameFile = null;
            if ($request->hasFile($inputName) && $request->file($inputName)->isValid()) {

                $size = $request->$inputName->getClientSize();

                if($size < 10000000) // 5 megas
                {
                    $name = uniqid(date('HisYmd'));
                    $extension = strtoupper($request->$inputName->extension());
                    if
                    (
                        $extension=='DOC'    ||
                        $extension=='XLSX'   ||
                        $extension=='PDF'    ||
                        $extension=='TXT'    ||
                        $extension=='JPG'    ||
                        $extension=='JPEG'   ||
                        $extension=='PNG'    ||
                        $extension=='XLS'    ||
                        $extension=='PPT'    ||
                        $extension=='PPTX'
                    )
                    {
                        $nameFile = "{$name}.{$extension}";
                    }
                    else
                    {
                        return null;
                    }
                    $upload = $request->$inputName->storeAs($path, $nameFile, 'public');

                    if ( !$upload )
                    {
                        return $nameFile;
                    }
                    else
                    {
                        return
                            [
                                'nameFile'      => $nameFile,
                                'sizeFile'      => $size,
                                'pathFile'      => $path,
                                'extensionFile' => $extension,
                            ];
                    }
                }
            }
            return $nameFile;
        }
    }
