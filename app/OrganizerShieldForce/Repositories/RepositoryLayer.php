<?php

    namespace App\OrganizerShieldforce\Repositories;

    use Illuminate\Http\Request;

    class RepositoryLayer
    {

        public function return(Request $request)
        {
            $model = new $request->model;
            if($request['action']=='view'){return $this->viewReturn($request, $model);}
            if($request['action']=='show'){return $this->showReturn($request, $model);}
            if($request['action']=='list'){return $this->listReturn($request, $model);}
            //----------------------------------------------------------------------------
            if($request['action']=='store'){return $this->storeReturn($request, $model);}
            if($request['action']=='update'){return $this->updateReturn($request, $model);}
            if($request['action']=='delete'){return $this->deleteReturn($request, $model);}
            return null;
        }

        public function viewReturn(Request $request, $model)
        {
            return
                [
                    'view'  => $request->route()->getName(),
                    'model' => isset($model) ? $model::all() : null
                ];
        }

        public function showReturn(Request $request, $model)
        {
            return
                [
                    'model' => isset($model) ? $model::find($request['id']) : null
                ];
        }

        public function listReturn(Request $request, $model)
        {
            return
                [
                    'model' => isset($model) ? $model::all() : null
                ];
        }

        public function storeReturn(Request $request, $model, array $save_img=null, array $save_file=null)
        {
            $rota = explode('.', $request->route()->getName());
            isset($model) ? $store = $model::create($request->all()) : $store = null;
            return
                [
                    'model'     => $store,
                    'redirect'  => $rota[0].".".$rota[1].".index"
                ];
        }

        public function updateReturn(Request $request, $model)
        {
        $update = $model::find($request->id);
        isset($update) ? $update->update($request->all()) : null;
        return
            [
                'model' => $update
            ];
        }

        public function deleteReturn(Request $request, $model)
        {
            $delete = $model::find($request->id);
            isset($delete) ? $delete->destroy($request->id) : null;
            return
                [
                    'model' => $delete
                ];
        }

    }
