<?php


    namespace App\OrganizerShieldForce\Controller;

    use App\OrganizerShieldForce\FunctionsGlobals\Configurations;
    use App\OrganizerShieldforce\Services\ServiceLayer;
    use Illuminate\Http\Request;

    trait ControllerTrait
    {
        /*
        *
        *  Variables Globals
        *
        */
        protected $request;
        protected $configurations;
        protected $service;
        protected $compactController;
        protected $model;


        //--------------------------------------------------------------------------------------------------------------
        /*
        *
        *  Método Contrutor
        *
        */
        public function __construct
        (
            Request                              $request,
            Configurations                       $configurations,
            ServiceLayer                         $service
        )
        {
            $this->request                       = $request;
            $this->configurations                = $configurations;
            $this->service                       = $service;
            $request['compactController']        = $this->getCompactController();
        }
        //--------------------------------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------------------------------
        /*
        *
        *  Métodos Set e Get CompactController
        *
        */
        public function setCompactController(array $data)
        {
            return $this->compactController = $data;
        }
        public function getCompactController()
        {
            return $this->compactController;
        }
        //--------------------------------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------------------------------
        /*
        *
        *  Métodos Set e Get Variável Model
        *
        */
        public function setModel($model)
        {
            $this->model = $model;
        }
        public function getModel()
        {
            return $this->model;
        }
        //--------------------------------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------------------------------
        /*
        *
        *  Métodos Para injetar validação, trocar menssagens de validação e variáveis globais
        *  Todos os métodos estão no model
        *
        *
        */
        public function validationsKeys(Request $request, array $keys)
        {
            return $request['validations'] = $keys;
        }
        public function validationsMessages(Request $request, array $messages)
        {
            return $request['messages'] = $messages;
        }
        public function variables(Request $request, array $variables)
        {
            return $request['variables'] = $variables;
        }
        //--------------------------------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------------------------------
        /*
        *
        *  Método para modificar o request de preciso
        *  Todos os métodos estão no model
        *
        *
        */
        public function modifyRequest(Request $request, array $modify)
        {
            return $request['modify'] = $modify;
        }
        //--------------------------------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------------------------------
        /*
        *
        *  Métodos para Ação (Esté é o método que retorna um VIEW)
        *  Todos os métodos estão no model
        *
        *
        */
        public function viewController(Request $request, $model, array $compact=[])
        {
            $modelApp = $this->returnObjModel($model);
            //-------------------------
            if(method_exists($modelApp, 'configController'))
            {
                $variables = $this->variables($request, $modelApp->configController()['variables']);
            }
            else
            {
                $variables = $this->variables($request, []);
            }
            //-------------------------
            $compactInternal = [
                'crud'     =>  $variables['crudName'],
                'title'    => env('APP_NAME') . " | " . $variables['crudName'],
            ];
            $variables_all = array_merge($compact, $variables, $compactInternal);
            //-------------------------
            $request['action']       = 'view';
            $request['model']        = $modelApp;
            //-------------------------
            $service = $this->service->return($request);
            //-------------------------
            if(isset($service['data']['view']))
            {
                return view($service['data']['view'], ['return'=>$service], $variables_all);
            }
            else
            {
                /*
                $crud           = 'Não Autorizado!';
                $codeError      = 401;
                $lineError      = 24;
                $fileError      = "App\OrganizerShieldForce\Responses\ResponseLayer";
                $imgError       = "Auth-Panel/images/unauthorized.png";
                $titleError     = 'Não Autorizado!';
                $textError      = "Você não tem permissão para acessar está área. Clique no botão para pedir autorização para o setor responsável!.";
                $action         = 'button';
                return response()->view('Panel.Errors.errorIntercept', compact('crud', 'titleError', 'codeError', 'imgError', 'textError', 'action', 'lineError', 'fileError'));
                */
                return back()->with('error', 'Acesso negado');
            }
        }
        /*
        *
        *  Métodos para Ação (Esté é o método que retorna um objeto de model escolhido)
        *  Todos os métodos estão no model
        *
        *
        */
        public function showController(Request $request, $model, $id)
        {
            $modelApp = $this->returnObjModel($model);
            //-------------------------
            $request['action']      = 'show';
            $request['model']       = $modelApp;
            $request['id']          = $id;
            //-------------------------
            $service = $this->service->return($request);
            //-------------------------
            return $service;
        }
        /*
        *
        *  Métodos para Ação (Esté é o método que retorna uma lista de objetos de model escolhido)
        *  Todos os métodos estão no model
        *
        *
        */
        public function listController(Request $request, $model)
        {
            $modelApp = $this->returnObjModel($model);
            //-------------------------
            $request['action']      = 'list';
            $request['model']       = $modelApp;
            //-------------------------
            $service = $this->service->return($request);
            //-------------------------
            return $service;
        }
        /*
        *
        *  Métodos para Ação (Esté é o método que salva no banco um objeto de model escolhido)
        *  Todos os métodos estão no model
        *
        *
        */
        public function storeController(Request $request, $model)
        {
            $modelApp = $this->returnObjModel($model);
            //-------------------------
            if(method_exists($modelApp, 'configController'))
            {
                $this->validationsKeys($request, $modelApp->configController()['store']);
                $this->validationsMessages($request, $modelApp->configController()['messages']);
            }
            else
            {
                $this->validationsKeys($request, []);
                $this->validationsMessages($request, []);
            }
            //-------------------------
            $request['action']      = 'store';
            $request['model']       = $modelApp;
            //-------------------------
            $service = $this->service->return($request);
            //-------------------------
            return $service;
        }
        /*
        *
        *  Métodos para Ação (Esté é o método que atualiza um objeto de model escolhido)
        *  Todos os métodos estão no model
        *
        *
        */
        public function updateController(Request $request, $model)
        {
            $modelApp = $this->returnObjModel($model);
            //-------------------------
            if(method_exists($modelApp, 'configController'))
            {
                $this->validationsKeys($request, $modelApp->configController()['update']);
                $this->validationsMessages($request, $modelApp->configController()['messages']);
            }
            else
            {
                $this->validationsKeys($request, []);
                $this->validationsMessages($request, []);
            }
            //-------------------------
            $request['action']      = 'update';
            $request['model']       = $modelApp;
            //-------------------------
            $service = $this->service->return($request);
            //-------------------------
            return $service;
        }
        /*
        *
        *  Métodos para Ação (Esté é o método que deleta um objeto de model escolhido)
        *  Todos os métodos estão no model
        *
        *
        */
        public function deleteController(Request $request, $model, $id)
        {
            $modelApp = $this->returnObjModel($model);
            //-------------------------
            if(method_exists($modelApp, 'configController'))
            {
                $this->validationsKeys($request, $modelApp->configController()['delete']);
                $this->validationsMessages($request, $modelApp->configController()['messages']);
            }
            else
            {
                $this->validationsKeys($request, []);
                $this->validationsMessages($request, []);
            }
            //-------------------------
            $request['action']      = 'delete';
            $request['model']       = $modelApp;
            $request['id']          = $id;
            //-------------------------
            $service = $this->service->return($request);
            //-------------------------
            return $service;
        }
        /*
        *
        *  Métodos para Ação (Esté é o método que retorna o OBJETO (Class pronta))
        *  Todos os métodos estão no model
        *
        *
        */
        public function returnObjModel($model)
        {
            $this->setModel($model);
            //-------------------------
            if($this->getModel()=='User' ? $class = "\\App\\".$this->getModel() : $class = "\\App\\Models\\".$this->getModel());
            //-------------------------
            if(class_exists($class) ? $modelApp = $class : $modelApp = null);
            //-------------------------
            $modelApp = new $class();
            //-------------------------
            return $modelApp;
        }
    }
