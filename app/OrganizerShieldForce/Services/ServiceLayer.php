<?php

    namespace App\OrganizerShieldforce\Services;

    use App\OrganizerShieldforce\Repositories\RepositoryLayer;
    use App\OrganizerShieldforce\Responses\ResponseLayer;
    use App\OrganizerShieldforce\Validators\ValidatorLayer;
    use Illuminate\Http\Request;

    class ServiceLayer
    {

        protected $validator;
        protected $repository;
        protected $response;

        public function __construct(ValidatorLayer $validator, RepositoryLayer $repository, ResponseLayer $response)
        {
            $this->validator = $validator;
            $this->repository = $repository;
            $this->response = $response;
        }

        public function return(Request $request)
        {
            $validator = $this->validator->return($request);
            if($validator->fails()==true)
            {
                return $this->response->return(null, $validator, $request);
            }
            return $this->response->return($this->repository->return($request), null, $request);
        }
    }
