<?php

    namespace App\OrganizerShieldforce\Responses;

    use App\OrganizerShieldforce\FunctionsGlobals\Configurations;
    use Illuminate\Http\Request;
    use Gate;

    class ResponseLayer
    {
        protected $request;
        //--------------------------
        protected $return;
        //-------------------------
        protected $configurations;

        public function __construct(Configurations $configurations)
        {
            $this->configurations = $configurations;
        }

        public function return($data, $validator, Request $request)
        {
            if(Gate::allows($request->route()->getName()))
            {
                return [
                    'code'               =>$this->returnParams($data, $validator)['return'][$request['action']]['code'],
                    'status'             =>$this->returnParams($data, $validator)['status'],
                    'msg'                =>$this->returnParams($data, $validator)['return'][$request['action']]['msg'],
                    'data'               =>$this->returnParams($data, $validator)['data'],
                ];
            }
            else
            {
                return [
                    'code'               =>401,
                    'status'             =>'unauthorized',
                    'msg'                =>'Não Autorizado',
                    'data'               =>null,
                ];
            }
        }

        public function returnParams($data, $validator)
        {
            $list_codes =
                [
                    'success'=>
                        [
                            'view'=>
                                [
                                    'code'=>1000,
                                    'msg'=>'Sucesso em visualizar View!',
                                ],
                            'index'=>
                                [
                                    'code'=>1000,
                                    'msg'=>'Sucesso em visualizar lista!',
                                ],
                            'list'=>
                                [
                                    'code'=>1000,
                                    'msg'=>'Sucesso em visualizar lista!',
                                ],
                            'show'=>
                                [
                                    'code'=>1001,
                                    'msg'=>'Sucesso em visualizar item!',
                                ],
                            'store'=>
                                [
                                    'code'=>1002,
                                    'msg'=>'Sucesso em criar item solicitado!',
                                ],
                            'update'=>
                                [
                                    'code'=>1003,
                                    'msg'=>'Sucesso em editar item solicitado!',
                                ],
                            'delete'=>
                                [
                                    'code'=>1004,
                                    'msg'=>'Sucesso em excluir item solicitado!',
                                ],
                            'create'=>
                                [
                                    'code'=>1007,
                                    'msg'=>'Sucesso em visualizar tela de cadastro!',
                                ],
                            'edit'=>
                                [
                                    'code'=>1008,
                                    'msg'=>'Sucesso em visualizar tela de edição!',
                                ],
                            'login'=>
                                [
                                    'code'=>1014,
                                    'msg'=>'Logado com sucesso!',
                                ],
                            'register'=>
                                [
                                    'code'=>1015,
                                    'msg'=>'Registrado com sucesso!',
                                ],
                        ],
                    'error'=>
                        [
                            'view'=>
                                [
                                    'code'=>2000,
                                    'msg'=>'Não foi possível visualizar View!',
                                ],
                            'index'=>
                                [
                                    'code'=>2000,
                                    'msg'=>'Não foi possível visualizar lista!',
                                ],
                            'list'=>
                                [
                                    'code'=>2000,
                                    'msg'=>'Não foi possível visualizar lista!',
                                ],
                            'show'=>
                                [
                                    'code'=>2001,
                                    'msg'=>'Não foi possível visualizar item!',
                                ],
                            'store'=>
                                [
                                    'code'=>2002,
                                    'msg'=>'Não foi possível criar item solicitado!',
                                ],
                            'update'=>
                                [
                                    'code'=>2003,
                                    'msg'=>'Não foi possível editar item solicitado!',
                                ],
                            'delete'=>
                                [
                                    'code'=>2004,
                                    'msg'=>'Não foi possível excluir item solicitado!',
                                ],
                            'create'=>
                                [
                                    'code'=>2007,
                                    'msg'=>'Não foi possível visualizar tela de cadastro!',
                                ],
                            'edit'=>
                                [
                                    'code'=>2008,
                                    'msg'=>'Não foi possível visualizar tela de edição!',
                                ],
                            'login'=>
                                [
                                    'code'=>2014,
                                    'msg'=>'Falha em realizar login!',
                                ],
                            'register'=>
                                [
                                    'code'=>2015,
                                    'msg'=>'Falha em realizar registro!',
                                ],
                        ]
                ];
            //----------------------------------------------------------------------------------------------------------
            if(isset($validator) && $validator->fails()==true)
            {
                return [
                    'status' => 'error',
                    'return' => $list_codes['error'],
                    'data'   => $validator->errors()
                ];
            }
            else
            {
                return [
                    'status' => 'success',
                    'return' => $list_codes['success'],
                    'data'   => $data
                ];
            }
        }
    }
