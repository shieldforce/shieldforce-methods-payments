<?php

namespace App\Http\Controllers\Home;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Gate;

class MainController extends Controller
{
    public function __construct()
    {

    }

    public function index()
    {
        //return redirect('/login');
        return view('Home.Main.index');
    }
}
