<?php

    namespace App\Http\Controllers\Panel\Users;

    use App\Models\Roles;
    use App\OrganizerShieldForce\Controller\ControllerInterface;
    use App\OrganizerShieldForce\Controller\ControllerTrait;
    use App\Http\Controllers\Controller;
    use App\User;
    use Illuminate\Http\Request;

    class UsersController extends Controller implements ControllerInterface
    {
        use ControllerTrait;

        /* -------------------------------------------------------------------------------------------------------------
        *
        *  @Função para página principal de cada Entidade
        *
        * ------------------------------------------------------------------------------------------------------------*/
        public function index(Request $request)
        {
            return $this->viewController($request, 'User', compact(''));
        }
        /*
        *
        *
        *
        * --------------------------------------------------------------------------------------------------------------
        *
        *  @Função para página ou consumo de API de 1 item da Entidade
        *
        * ------------------------------------------------------------------------------------------------------------*/
        public function show(Request $request, $id)
        {
            return $this->showController($request, 'User', $id);
        }
        /*
        *
        *
        *
        * --------------------------------------------------------------------------------------------------------------
        *
        *  @Função para página ou consumo de API de lista da Entidade
        *
        * ------------------------------------------------------------------------------------------------------------*/
        public function list(Request $request)
        {
            return $this->listController($request, 'User');
        }
        /*
        *
        *
        *
        * --------------------------------------------------------------------------------------------------------------
        *
        *  @Função para página ou consumo de API de criação de item da Entidade
        *
        * ------------------------------------------------------------------------------------------------------------*/
        public function create(Request $request)
        {
            $roles = Roles::where('id', '!=', 0)->get();
            return $this->viewController($request, 'User', compact('roles'));
        }
        /*
        *
        *
        *
        * --------------------------------------------------------------------------------------------------------------
        *
        *  @Função para salvar um item da Entidade
        *
        * ------------------------------------------------------------------------------------------------------------*/
        public function store(Request $request)
        {
            if($request['password']!=null)
            {
                $this->modifyRequest($request, [
                    $request['password'] =  bcrypt($request['password']),
                ]);
            }
            $return = $this->storeController($request, 'User');
            if($return['status']=='error')
            {
                return back()->with($return['status'], $return['msg'])->with('data', $return['data'])->withInput()->withErrors($return['data']);
            }
            $return['data']['model']->roles()->sync($request['roles_ids']);
            return redirect()->route($return['data']['redirect'])->with($return['status'], $return['msg']);
        }
        /*
        *
        *
        *
        * --------------------------------------------------------------------------------------------------------------
        *
        *  @Função para página ou consumo de API de edição de item da Entidade
        *
        * ------------------------------------------------------------------------------------------------------------*/
        public function edit(Request $request, $id)
        {
            $roles = Roles::where('id', '!=', 0)->get();
            $model = User::find($id);
            return $this->viewController($request, 'User', compact('roles', 'model'));
        }
        /*
        *
        *
        *
        * --------------------------------------------------------------------------------------------------------------
        *
        *  @Função para atualizar um item da Entidade
        *
        * ------------------------------------------------------------------------------------------------------------*/
        public function update(Request $request)
        {
            $model = User::find($request['id']);
            if($model->password!=$request['password']){
                $this->modifyRequest($request, [
                    $request['password'] =  bcrypt($request['password'])
                ]);
            }

            $return = $this->updateController($request, 'User');

            if($return['status']=='error')
            {
                return back()->with($return['status'], $return['msg'])->with('data', $return['data'])->withInput()->withErrors($return['data']);
            }
            $return['data']['model']->roles()->sync($request['roles_ids']);
            return back()->with($return['status'], $return['msg']);
        }
        /*
        *
        *
        *
        * --------------------------------------------------------------------------------------------------------------
        *
        *  @Função para deletar um item da Entidade
        *
        * ------------------------------------------------------------------------------------------------------------*/
        public function delete(Request $request, $id)
        {
            $return = $this->deleteController($request, 'User', $id);

            if($return['status']=='error')
            {
                return back()->with($return['status'], $return['msg'])->with('data', $return['data'])->withInput()->withErrors($return['data']);
            }
            return back()->with($return['status'], $return['msg']);

        }
        /*
        *
        *
        *
        * ------------------------------------------------------------------------------------------------------------*/

    }
