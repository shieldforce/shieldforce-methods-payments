<?php

    namespace App\Http\Controllers\Panel\Roles;

    use App\Models\Permissions;
    use App\Models\Roles;
    use App\OrganizerShieldForce\Controller\ControllerInterface;
    use App\OrganizerShieldForce\Controller\ControllerTrait;
    use App\Http\Controllers\Controller;
    use Illuminate\Http\Request;

    class RolesController extends Controller implements ControllerInterface
    {
        use ControllerTrait;

        /* -------------------------------------------------------------------------------------------------------------
        *
        *  @Função para página principal de cada Entidade
        *
        * ------------------------------------------------------------------------------------------------------------*/
        public function index(Request $request)
        {
            return $this->viewController($request, 'Roles', compact(''));
        }
        /*
        *
        *
        *
        * --------------------------------------------------------------------------------------------------------------
        *
        *  @Função para página ou consumo de API de 1 item da Entidade
        *
        * ------------------------------------------------------------------------------------------------------------*/
        public function show(Request $request, $id)
        {
            return $this->showController($request, 'Roles', $id);
        }
        /*
        *
        *
        *
        * --------------------------------------------------------------------------------------------------------------
        *
        *  @Função para página ou consumo de API de lista da Entidade
        *
        * ------------------------------------------------------------------------------------------------------------*/
        public function list(Request $request)
        {
            return $this->listController($request, 'Roles');
        }
        /*
        *
        *
        *
        * --------------------------------------------------------------------------------------------------------------
        *
        *  @Função para página ou consumo de API de criação de item da Entidade
        *
        * ------------------------------------------------------------------------------------------------------------*/
        public function create(Request $request)
        {
            $permissions = Permissions::where('id', '!=', 0)->get();
            $groups = Permissions::distinct()->select('group')->get();
            return $this->viewController($request, 'Roles', compact('groups', 'permissions'));
        }
        /*
        *
        *
        *
        * --------------------------------------------------------------------------------------------------------------
        *
        *  @Função para salvar um item da Entidade
        *
        * ------------------------------------------------------------------------------------------------------------*/
        public function store(Request $request)
        {
            $return = $this->storeController($request, 'Roles');
            if($return['status']=='error')
            {
                return back()->with($return['status'], $return['msg'])->with('data', $return['data'])->withInput()->withErrors($return['data']);
            }
            $return['data']['model']->permissions()->sync($request['permissions_ids']);
            return redirect()->route($return['data']['redirect'])->with($return['status'], $return['msg']);
        }
        /*
        *
        *
        *
        * --------------------------------------------------------------------------------------------------------------
        *
        *  @Função para página ou consumo de API de edição de item da Entidade
        *
        * ------------------------------------------------------------------------------------------------------------*/
        public function edit(Request $request, $id)
        {
            $permissions = Permissions::where('id', '!=', 0)->get();
            $groups = Permissions::distinct()->select('group')->get();
            $model = Roles::find($id);
            return $this->viewController($request, 'Roles', compact('permissions', 'groups', 'model'));
        }
        /*
        *
        *
        *
        * --------------------------------------------------------------------------------------------------------------
        *
        *  @Função para atualizar um item da Entidade
        *
        * ------------------------------------------------------------------------------------------------------------*/
        public function update(Request $request)
        {
            $return = $this->updateController($request, 'Roles');
            if($return['status']=='error')
            {
                return back()->with($return['status'], $return['msg'])->with('data', $return['data'])->withInput()->withErrors($return['data']);
            }
            $return['data']['model']->permissions()->sync($request['permissions_ids']);
            return back()->with($return['status'], $return['msg']);
        }
        /*
        *
        *
        *
        * --------------------------------------------------------------------------------------------------------------
        *
        *  @Função para deletar um item da Entidade
        *
        * ------------------------------------------------------------------------------------------------------------*/
        public function delete(Request $request, $id)
        {
            $return = $this->deleteController($request, 'Roles', $id);

            if($return['status']=='error')
            {
                return back()->with($return['status'], $return['msg'])->with('data', $return['data'])->withInput()->withErrors($return['data']);
            }
            return back()->with($return['status'], $return['msg']);

        }
        /*
        *
        *
        *
        * ------------------------------------------------------------------------------------------------------------*/

    }
