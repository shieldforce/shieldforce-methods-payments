<?php

namespace App\Http\Controllers\Panel\Main;

use App\OrganizerShieldForce\Controller\ControllerTrait;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Gate;

class MainController extends Controller
{
    use ControllerTrait;

    public function index(Request $request)
    {
        if(Gate::allows("Panel.Main.index"))
        {
            return $this->viewController($request, 'MainPanel', compact(''));
        }
        else
        {
            Auth::logout();
            return back()->with('error', 'Acesso negado')->withErrors('error', 'Acesso negado');
        }
    }
}
