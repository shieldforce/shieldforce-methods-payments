<?php

    namespace App\Http\Controllers\Panel\Permissions;

    use App\Models\Permissions;
    use App\Models\Roles;
    use App\OrganizerShieldForce\Controller\ControllerInterface;
    use App\OrganizerShieldForce\Controller\ControllerTrait;
    use App\Http\Controllers\Controller;
    use Illuminate\Http\Request;

    class PermissionsController extends Controller implements ControllerInterface
    {
        use ControllerTrait;

        /* -------------------------------------------------------------------------------------------------------------
        *
        *  @Função para página principal de cada Entidade
        *
        * ------------------------------------------------------------------------------------------------------------*/
        public function index(Request $request)
        {
            return $this->viewController($request, 'Permissions', compact(''));
        }
        /*
        *
        *
        *
        * --------------------------------------------------------------------------------------------------------------
        *
        *  @Função para página ou consumo de API de 1 item da Entidade
        *
        * ------------------------------------------------------------------------------------------------------------*/
        public function show(Request $request, $id)
        {
            return $this->showController($request, 'Permissions', $id);
        }
        /*
        *
        *
        *
        * --------------------------------------------------------------------------------------------------------------
        *
        *  @Função para página ou consumo de API de lista da Entidade
        *
        * ------------------------------------------------------------------------------------------------------------*/
        public function list(Request $request)
        {
            return $this->listController($request, 'Permissions');
        }
        /*
        *
        *
        *
        * --------------------------------------------------------------------------------------------------------------
        *
        *  @Função para página ou consumo de API de criação de item da Entidade
        *
        * ------------------------------------------------------------------------------------------------------------*/
        public function create(Request $request)
        {
            $roles = Roles::where('id', '!=', 0)->get();
            $groups = Roles::distinct()->select('group')->get();
            return $this->viewController($request, 'Permissions', compact('groups', 'roles'));
        }
        /*
        *
        *
        *
        * --------------------------------------------------------------------------------------------------------------
        *
        *  @Função para salvar um item da Entidade
        *
        * ------------------------------------------------------------------------------------------------------------*/
        public function store(Request $request)
        {
            $return = $this->storeController($request, 'Permissions');
            if($return['status']=='error')
            {
                return back()->with($return['status'], $return['msg'])->with('data', $return['data'])->withInput()->withErrors($return['data']);
            }
            $return['data']['model']->roles()->sync($request['roles_ids']);
            return redirect()->route($return['data']['redirect'])->with($return['status'], $return['msg']);
        }
        /*
        *
        *
        *
        * --------------------------------------------------------------------------------------------------------------
        *
        *  @Função para página ou consumo de API de edição de item da Entidade
        *
        * ------------------------------------------------------------------------------------------------------------*/
        public function edit(Request $request, $id)
        {
            $roles = Roles::where('id', '!=', 0)->get();
            $groups = Roles::distinct()->select('group')->get();
            $model = Permissions::find($id);
            return $this->viewController($request, 'Permissions', compact('roles', 'groups', 'model'));
        }
        /*
        *
        *
        *
        * --------------------------------------------------------------------------------------------------------------
        *
        *  @Função para atualizar um item da Entidade
        *
        * ------------------------------------------------------------------------------------------------------------*/
        public function update(Request $request)
        {
            $return = $this->updateController($request, 'Permissions');
            if($return['status']=='error')
            {
                return back()->with($return['status'], $return['msg'])->with('data', $return['data'])->withInput()->withErrors($return['data']);
            }
            $return['data']['model']->roles()->sync($request['roles_ids']);
            return back()->with($return['status'], $return['msg']);
        }
        /*
        *
        *
        *
        * --------------------------------------------------------------------------------------------------------------
        *
        *  @Função para deletar um item da Entidade
        *
        * ------------------------------------------------------------------------------------------------------------*/
        public function delete(Request $request, $id)
        {
            $return = $this->deleteController($request, 'Permissions', $id);

            if($return['status']=='error')
            {
                return back()->with($return['status'], $return['msg'])->with('data', $return['data'])->withInput()->withErrors($return['data']);
            }
            return back()->with($return['status'], $return['msg']);

        }
        /*
        *
        *
        *
        * ------------------------------------------------------------------------------------------------------------*/

    }
