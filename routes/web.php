<?php

    Auth::routes();

    View::composer('*', function($view)
    {
        $routeActive = Route::currentRouteName();
        $route = explode('.', $routeActive);
        $view
            ->with('routeActive', $routeActive)
            ->with('route', $route);
    });

    //Group Home
    Route::namespace('Home')->name('Home.')->group(function (){

        //Name Main
        Route::name('Main.')->group(function (){

            //Route Index
            Route::get('/', 'MainController@index')->name('index');

        });

    });

    //Group Panel
    Route::middleware('auth')->prefix('painel')->namespace('Panel')->name('Panel.')->group(function (){

        //Name Main
        Route::prefix('principal')->namespace('Main')->name('Main.')->group(function (){

            //Route Index
            Route::get('index', 'MainController@index')->name('index');

        });

        //Group Users
        Route::prefix('usuarios')->namespace('Users')->name('Users.')->group(function (){

            //Route Index
            Route::get('index', 'UsersController@index')->name('index');
            //Route Show
            Route::get('/{id?}/show', 'UsersController@show')->name('show');
            //Route List
            Route::get('list', 'UsersController@list')->name('list');
            //Route Create
            Route::get('create', 'UsersController@create')->name('create');
            //Route Store
            Route::post('store', 'UsersController@store')->name('store');
            //Route Edit
            Route::get('{id?}/edit', 'UsersController@edit')->name('edit');
            //Route Update
            Route::post('update', 'UsersController@update')->name('update');
            //Route Delete
            Route::get('{id?}/delete', 'UsersController@delete')->name('delete');

        });

        //Group Roles
        Route::prefix('funcoes')->namespace('Roles')->name('Roles.')->group(function (){

            //Route Index
            Route::get('index', 'RolesController@index')->name('index');
            //Route Show
            Route::get('/{id?}/show', 'RolesController@show')->name('show');
            //Route List
            Route::get('list', 'RolesController@list')->name('list');
            //Route Create
            Route::get('create', 'RolesController@create')->name('create');
            //Route Store
            Route::post('store', 'RolesController@store')->name('store');
            //Route Edit
            Route::get('{id?}/edit', 'RolesController@edit')->name('edit');
            //Route Update
            Route::post('update', 'RolesController@update')->name('update');
            //Route Delete
            Route::get('{id?}/delete', 'RolesController@delete')->name('delete');

        });

        //Group Permissions
        Route::prefix('permissoes')->namespace('Permissions')->name('Permissions.')->group(function (){

            //Route Index
            Route::get('index', 'PermissionsController@index')->name('index');
            //Route Show
            Route::get('/{id?}/show', 'PermissionsController@show')->name('show');
            //Route List
            Route::get('list', 'PermissionsController@list')->name('list');
            //Route Create
            Route::get('create', 'PermissionsController@create')->name('create');
            //Route Store
            Route::post('store', 'PermissionsController@store')->name('store');
            //Route Edit
            Route::get('{id?}/edit', 'PermissionsController@edit')->name('edit');
            //Route Update
            Route::post('update', 'PermissionsController@update')->name('update');
            //Route Delete
            Route::get('{id?}/delete', 'PermissionsController@delete')->name('delete');

        });

});
